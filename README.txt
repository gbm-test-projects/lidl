Instructions:
1. Clone the repo from GitLab
2. Download all Maven sources and run a maven build
3a. Run tests locally by running them individually form their classes
(i.e. LoginPositiveTests) or by running theFETests.xml in the
resources package
3b. Run/view previous runs in GitLab -> Pipelines