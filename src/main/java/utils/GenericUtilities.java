package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.util.*;
import java.util.List;

public class GenericUtilities {

    public void optionalWaitForElementVisibility(WebDriverWait wait, By by) {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean isElementVisible(WebDriverWait wait, By by) {
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementEnabled(WebDriverWait wait, By by) {
        boolean enabled = false;
        try {
            if (wait.until(ExpectedConditions.visibilityOfElementLocated(by)).isEnabled()) {
                enabled = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return enabled;
    }

    public boolean isElementPresent(WebDriverWait wait, By by) {
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void optionallyWaitUntilElementTextChanges(WebDriverWait wait, By by) {
        String textToWaitFor = wait.until(ExpectedConditions.visibilityOfElementLocated(by)).getText();
        try {
            wait.until(ExpectedConditions.not(ExpectedConditions.textToBe(by, textToWaitFor)));
        } catch (Exception e) { }
    }

    public void selectItemFromDropdownBy(String item, WebDriverWait wait, By by) {
        List<WebElement> items = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                by));
        for (WebElement itemInDropdown : items) {
            if (itemInDropdown.getText().equals(item)) {
                itemInDropdown.click();
                wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
                return;
            }
        }
    }

    public String getCurrentYear() {
        return Integer.toString(LocalDate.now().getYear());
    }

    public String getCurrentMonth() {
        return LocalDate.now().getMonth().toString();
    }

    public boolean isStringListSorted(ArrayList<String> array) {
        String[] arraySorted = removeCapitalization(array).toArray(new String[0]);
        Arrays.sort(arraySorted);
        List<String> sortedArrayList = Arrays.asList(arraySorted);
        return sortedArrayList.equals(removeCapitalization(array));
    }

    public boolean isStringListReverseSorted(ArrayList<String> array)  {
        String[] arraySorted = removeCapitalization(array).toArray(new String[0]);
        Arrays.sort(arraySorted, Collections.reverseOrder());
        List<String> sortedArrayList = Arrays.asList(arraySorted);
        return sortedArrayList.equals(removeCapitalization(array));
    }

    public ArrayList<String> removeCapitalization(ArrayList<String> array) {
        ArrayList<String> lowerCaseList = new ArrayList<>();
        for (String string : array) {
            lowerCaseList.add(string.toLowerCase());
        }
        return lowerCaseList;
    }

    public boolean isIntListSorted(ArrayList<Integer> array) {
        boolean ascending = true;
        for (int i = 1; i < array.size(); i++) {
            ascending = ascending && array.get(i) >= array.get(i - 1);
        }
        return ascending;
    }

    public boolean isDoubleListSorted(ArrayList<Double> array) {
        boolean ascending = true;
        for (int i = 1; i < array.size(); i++) {
            ascending = ascending && array.get(i) >= array.get(i - 1);
        }
        return ascending;
    }

    public boolean isIntListReverseSorted(ArrayList<Integer> array)  {
        boolean ascending = true;
        for (int i = 1; i < array.size(); i++) {
            ascending = ascending && array.get(i) <= array.get(i - 1);
        }
        return ascending;
    }

    public boolean isDoubleListReverseSorted(ArrayList<Double> array) {
        boolean ascending = true;
        for (int i = 1; i < array.size(); i++) {
            ascending = ascending && array.get(i) <= array.get(i - 1);
        }
        return ascending;
    }

    public ArrayList<String> getStringListFromWebElementList(By by, WebDriverWait wait) {
        ArrayList<String> listToReturn = new ArrayList<>();
        for (WebElement element : wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                by))) {
            listToReturn.add(element.getText().replaceAll("[^a-zA-Z0-9]", ""));
        }
        return listToReturn;
    }

    public ArrayList<Integer> getIntListFromWebElementList(By by, WebDriverWait wait) {
        ArrayList<Integer> listToReturn = new ArrayList<>();
        for (WebElement element : wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                by))) {
            listToReturn.add(Integer.parseInt(element.getText().replaceAll("[^a-zA-Z0-9]", "")));
        }
        return listToReturn;
    }

    public ArrayList<Double> getDoubleListFromWebElementList(By by, WebDriverWait wait) {
        ArrayList<Double> listToReturn = new ArrayList<>();
        for (WebElement element : wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                by))) {
            listToReturn.add(Double.parseDouble(element.getText()));
        }
        return listToReturn;
    }

    public String splitTextAtFirstSymbol(String text) {
        return text.split("[^a-zA-Z0-9]")[0];
    }

    public void savageClearElement(WebElement element) {
        element.click();
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_A);
            robot.keyRelease(KeyEvent.VK_A);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_DELETE);
        } catch (Exception e) {

        }
    }

    public void savageClear() {
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_A);
            robot.keyRelease(KeyEvent.VK_A);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_DELETE);
        } catch (Exception e) {

        }
    }

    public void selectOrDeselectAllCheckboxes(WebDriverWait wait, By by, int lockedBoxes) {
        List<WebElement> checkboxes = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                by));
        int i = 0;
        int y = 0;
        for (WebElement element : checkboxes) {
            if (!element.isSelected() && element.isEnabled()) {
                i++;
            } else if (element.isSelected() && element.isEnabled()) {
                y++;
            }
        }
        for (WebElement element : checkboxes) {
            if (i <= checkboxes.size()-lockedBoxes && !element.isSelected() && element.isEnabled()) {
                element.click();
            } else if (y == checkboxes.size()-lockedBoxes && element.isSelected() && element.isEnabled()) {
                element.click();
            }
        }
    }

    public boolean zeroSumCheck(int first, int second, int difference) {
        boolean zeroSum = false;
        int i = 0;
        int y = 0;
        if (first + difference == second) {
            i++;
        }
        if (second + difference == first) {
            y++;
        }
        if (first == second) {
            i++;
            y++;
        }
        if (i == 1 && y == 0 || y == 1 && i == 0 || y == 2 && i == 2) {
            zeroSum = true;
        }
        return zeroSum;
    }

    public void refreshPage(WebDriver driver) {
        driver.navigate().refresh();
    }
}