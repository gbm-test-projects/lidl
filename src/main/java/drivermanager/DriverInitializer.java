package drivermanager;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverInitializer extends EnvironmentSetup{
    private WebDriver driver;

    public void initializeDriver() {
            if (getConfigReader().getBrowser().equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                options.addArguments("enable-automation");
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-infobars");
                options.addArguments("--disable-browser-side-navigation");
                options.addArguments("--disable-gpu");
                options.addArguments("--disable-dev-shm-usage");
                options.addArguments("--remote-allow-origins=*");
                if(getConfigReader().getHeadless().equals("true")) {
                    options.addArguments("--allow-insecure-localhost");
                    options.addArguments("--window-size=1920,1080");
                    options.addArguments("--headless");
                }
                driver = new ChromeDriver(options);
                driver.navigate().to(getUrl());
            } else if (getConfigReader().getBrowser().equals("firefox")) {
                driver = new FirefoxDriver();
            }
    }

    public WebDriver getDriver() {
        return driver;
    }
}
