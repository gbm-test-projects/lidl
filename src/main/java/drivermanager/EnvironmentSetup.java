package drivermanager;

import java.io.IOException;

public class EnvironmentSetup {
    private String url;
    private ConfigReader configReader;
    private String simpleUrl = "lidl.de";
    private String protocol;


    public EnvironmentSetup() {
        initConfigReader();
        configureProtocol();
        configureURL();
    }

    private void configureURL() {
        switch (configReader.getEnv()) {
            case "dev":
                setUrl("");
                break;
            case "stage":
                setUrl(getProtocol() + getSimpleUrl());
                break;
            case "prod":
                setUrl("");
                break;
        }
    }

    private void configureProtocol() {
        switch (getConfigReader().getProtocol()) {
            case "http":
                setProtocol("http://");
                break;
            case "https":
                setProtocol("https://");
                break;
        }
    }

    private void initConfigReader() {
        try {
            configReader = new ConfigReader();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public String getUrl() {
        return url;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getSimpleUrl() {
        return simpleUrl;
    }

    private void setUrl(String url) {
        this.url = url;
    }

    private void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    private void setSimpleUrl(String simpleUrl) {
        this.simpleUrl = simpleUrl;
    }

    public ConfigReader getConfigReader() {
        return configReader;
    }
}
