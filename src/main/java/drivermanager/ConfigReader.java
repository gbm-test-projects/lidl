package drivermanager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
    private String browser;
    private String env;
    private String tech;
    private String os;
    private String protocol;
    private String headless;

    public ConfigReader() throws IOException {
        if(getSystemProperty("browser")!=null) {
            this.setBrowser(getSystemProperty("browser"));
        } else {
            this.setBrowser(getProperty("browser"));
        }
        if(getSystemProperty("env")!=null) {
            this.setEnv(getSystemProperty("env"));
        } else {
            this.setEnv(getProperty("env"));
        }
        if(getSystemProperty("tech")!=null) {
            this.setTech(getSystemProperty("tech"));
        } else {
            this.setTech(getProperty("tech"));
        }
        if(getSystemProperty("os")!=null) {
            this.setOs(getSystemProperty("os"));
        } else {
            this.setOs(getProperty("os"));
        }
        if(getSystemProperty("protocol")!=null) {
            this.setProtocol(getSystemProperty("protocol"));
        } else {
            this.setProtocol(getProperty("protocol"));
        }
        if(getSystemProperty("headless")!=null) {
            this.setHeadless(getSystemProperty("headless"));
        } else {
            this.setHeadless(getProperty("headless"));
        }
    }

    protected String getProperty(String property) throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);

        return prop.getProperty(property);
    }

    protected String getSystemProperty(String systemPropName) {
        return System.getProperty(systemPropName);
    }

    public String getBrowser() {
        return browser;
    }

    public String getEnv() {
        return env;
    }

    public String getTech() { return tech; }

    public String getOs() { return os; }

    public String getProtocol() {return protocol;}

    public String getHeadless() {
        return headless;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public void setTech(String tech) { this.tech = tech; }

    public void setOs(String os) { this.os = os; }

    public void setProtocol(String protocol) { this.protocol = protocol; }

    public void setHeadless(String headless) {
        this.headless = headless;
    }
}
