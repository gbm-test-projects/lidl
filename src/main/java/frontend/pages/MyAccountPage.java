package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class MyAccountPage extends AbstractPageObject {
    public static final String MY_ORDERS_BUTTON_XPATH =
            "(//div[@id='uniqueAccount_profileAccountMenu_titleOnlineOrders'])[1]";

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public void wasLoginSuccessful() {
        Assert.assertTrue(getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(MY_ORDERS_BUTTON_XPATH))).isDisplayed(),
                "The 'My Orders' section was supposed to be displayed, but it wasn't");
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(MY_ORDERS_BUTTON_XPATH)));
    }
}
