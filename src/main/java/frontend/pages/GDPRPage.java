package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GDPRPage extends AbstractPageObject {
    private static final String REJECT_BUTTON_ID = "onetrust-reject-all-handler";

    public GDPRPage(WebDriver driver) {
        super(driver);
    }

    public HeaderPage clickRejectCookies() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(REJECT_BUTTON_ID))).click();
        return new HeaderPage(getDriver());
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(REJECT_BUTTON_ID)));
    }
}
