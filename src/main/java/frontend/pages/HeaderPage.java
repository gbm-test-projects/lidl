package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderPage extends AbstractPageObject {
    private static final String SEARCH_BOX_ID = "s-search-input-field";
    private static final String MY_ACCOUNT_BUTTON_XPATH = "(//a[@data-ga-action='My Account'])[1]";

    public HeaderPage(WebDriver driver) {
        super(driver);
    }

    public HeaderPage navigateHome() {
        getDriver().navigate().to(getUrl());

        return new HeaderPage(getDriver());
    }

    public LoginEmailPage clickOnMyAccount() {
        getWait().until(ExpectedConditions.elementToBeClickable(
                By.xpath(MY_ACCOUNT_BUTTON_XPATH))).click();
        return new LoginEmailPage(getDriver());
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(SEARCH_BOX_ID)));
    }
}
