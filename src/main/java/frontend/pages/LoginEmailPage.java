package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class LoginEmailPage extends AbstractPageObject {
    private static final String EMAIL_OR_PHONE_LOGIN_FIELD_ID = "field_EmailOrPhone";
    private static final String CONTINUE_BUTTON_ID = "button_btn_submit_email";
    private static final String EMAIL_ERROR_CSS = ".error_EmailOrPhone";

    public LoginEmailPage(WebDriver driver) {
        super(driver);
    }

    public LoginEmailPage typeInEmailField(String emailOrPhone) {
        WebElement field = getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(EMAIL_OR_PHONE_LOGIN_FIELD_ID)));
        field.clear();
        field.sendKeys(emailOrPhone);

        return new LoginEmailPage(getDriver());
    }

    public LoginPasswordPage clickContinue() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(CONTINUE_BUTTON_ID))).click();

        return new LoginPasswordPage(getDriver());
    }

    public LoginEmailPage clickContinueWithError() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(CONTINUE_BUTTON_ID))).click();

        return new LoginEmailPage(getDriver());
    }

    public void isEmailErrorDisplayed() {
        Assert.assertTrue(getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(CONTINUE_BUTTON_ID))).isDisplayed(), "Error message was not displayed");
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(CONTINUE_BUTTON_ID)));
    }
}
