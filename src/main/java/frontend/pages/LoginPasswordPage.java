package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class LoginPasswordPage extends AbstractPageObject {
    private static final String PASSWORD_FIELD_ID = "field_Password";
    private static final String CONTINUE_BUTTON_XPATH = "//button[@id='button_submit']";

    public LoginPasswordPage(WebDriver driver) {
        super(driver);
    }

    public LoginPasswordPage typeInPasswordField(String password) {
        WebElement field = getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.id(PASSWORD_FIELD_ID)));
        field.clear();
        field.sendKeys(password);

        return new LoginPasswordPage(getDriver());
    }

    public LoginPasswordPage clickContinueWithError() {
        getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(CONTINUE_BUTTON_XPATH))).click();

        return new LoginPasswordPage(getDriver());
    }

    public MyAccountPage clickContinue() {
        getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(CONTINUE_BUTTON_XPATH))).click();

        return new MyAccountPage(getDriver());
    }

    public void isPasswordPageStillDisplayed() {
        Assert.assertTrue(getWait().until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(CONTINUE_BUTTON_XPATH))).isDisplayed(),
                "The login was successful with a wrong Password.");
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CONTINUE_BUTTON_XPATH)));
    }
}
