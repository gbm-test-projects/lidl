package lidltests.login;

import drivermanager.DriverInitializer;
import frontend.pages.GDPRPage;
import frontend.pages.HeaderPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static utils.TestVariables.*;

public class LoginPositiveTests extends DriverInitializer {
    HeaderPage headerPage;
    GDPRPage gdprPage;

    @BeforeClass
    public void setup() {
        initializeDriver();
        gdprPage = new GDPRPage(getDriver());
        headerPage = gdprPage.clickRejectCookies();
    }

    @AfterClass
    private void tearDown(){
        getDriver().quit();
    }

    @Test
    public void verifyLoginTest() {
        headerPage.clickOnMyAccount()
                .typeInEmailField(USER_EMAIL)
                .clickContinue()
                .typeInPasswordField(PASSWORD)
                .clickContinue()
                .wasLoginSuccessful();
    }
}
