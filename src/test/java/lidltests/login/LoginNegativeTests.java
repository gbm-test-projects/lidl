package lidltests.login;

import drivermanager.DriverInitializer;
import frontend.pages.GDPRPage;
import frontend.pages.HeaderPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.*;

import static utils.TestVariables.*;

public class LoginNegativeTests extends DriverInitializer {
    HeaderPage headerPage;
    GDPRPage gdprPage;

    @BeforeClass
    public void setup() {
        initializeDriver();
        gdprPage = new GDPRPage(getDriver());
        headerPage = gdprPage.clickRejectCookies();
    }

    @BeforeMethod
    private void cleanup() {
        headerPage.navigateHome();
    }

    @AfterClass
    private void tearDown(){
        getDriver().quit();
    }

    @Test
    public void loginWithBadEmailTest() {
        headerPage.clickOnMyAccount()
                .typeInEmailField(RandomStringUtils.randomAlphabetic(10))
                .clickContinueWithError()
                .isEmailErrorDisplayed();
    }

    // Could be a failure if business case required an easily visible fail state.
    @Test
    public void loginWithBadPasswordTest() {
        headerPage.clickOnMyAccount()
                .typeInEmailField(USER_EMAIL)
                .clickContinue()
                .typeInPasswordField(RandomStringUtils.randomAlphabetic(10))
                .clickContinueWithError()
                .isPasswordPageStillDisplayed();
    }
}
